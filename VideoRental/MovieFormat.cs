﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VideoRental
{
    public enum MovieFormat
    {
        Childrens = 1,
        Regular = 2,
        Newest = 3
    }
}
