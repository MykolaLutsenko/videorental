﻿using System;

namespace VideoRental
{
    public class Rental
    {
        private Movie movie;
        private int days;

        public int Days
        {
            get { return days;}
            private set
            {
                if (value < 0)
                    throw new RentalDaysException(value);

                days = value;
            }
        }

        public Rental(Movie movie, int days)
        {
            this.movie = movie;
            Days = days;
        }

        public double CalculateDebt()
        {
            return movie.RentalPrice * Days;
        }

        public void AddRentalDays(int days = 1)
        {
            Days += days;
        }

        public void SubstructRentalDays(int days = 1)
        {
            Days -= days;
        }
    }
}