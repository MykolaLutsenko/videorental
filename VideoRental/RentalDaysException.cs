﻿using System;
using System.Runtime.Serialization;

namespace VideoRental
{
    [Serializable]
    public class RentalDaysException : Exception
    {
        public double Days { get; set; }

        public RentalDaysException()
        {
        }

        public RentalDaysException(int days)
        {
            Days = days;
        }

        public RentalDaysException(string message) : base(message)
        {
        }

        public RentalDaysException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RentalDaysException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        
    }
}