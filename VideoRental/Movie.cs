﻿namespace VideoRental
{
    public class Movie
    {
        private double rentalPrice;

        public double RentalPrice { get; set; }

        public Movie(double rentalPrice) 
        {
            this.RentalPrice = rentalPrice;
        }
    }
}