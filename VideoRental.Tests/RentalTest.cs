using System;
using NUnit.Framework;

namespace VideoRental.Tests
{
    [TestFixture]
    public class RentalTest
    {
        //��� ������ _ �������� �������������� _ ��������� ���������

        [TestCase(MovieFormat.Regular, 6, ExpectedResult = 12)]
        [TestCase(MovieFormat.Childrens, 6, ExpectedResult = 6)]
        [TestCase(MovieFormat.Newest, 5, ExpectedResult = 15)]
        [TestCase(MovieFormat.Newest, 7, ExpectedResult = 21)]
        public static double CalculateDebt_2RentalPrice6Days_12Return(int a, int b)
        {
            //double rentalPrice = 2;
            //int days = 6;

            Movie movie = new Movie(rentalPrice: a);
            Rental rental = new Rental(movie, days: b);

            return rental.CalculateDebt();

            //Assert.AreEqual((days * rentalPrice), rental.CalculateDebt());


        }

        [Test]
        public static void AddRentalDays_CallWithoutParameter_AddOneDay()
        {
            Rental rental = new Rental(new Movie(rentalPrice: 2), days:6);

            rental.AddRentalDays();
            Assert.AreEqual(2*7,rental.CalculateDebt());

            
        }

        [Test]
        public static void AddRentalDays_CallWithParameter_AddParametersDaysCount()
        {
            Rental rental = new Rental(new Movie(rentalPrice: 2),days: 6);

            rental.AddRentalDays(2);
            Assert.AreEqual(2 * 8, rental.CalculateDebt());
        }

        [Test]
        public static void SubstructRentalDays_Substruct_Substruct()
        {
            Rental rental = new Rental(new Movie(rentalPrice: 2),days: 6);

            rental.SubstructRentalDays();
            Assert.AreEqual(2*5, rental.CalculateDebt());

            rental.SubstructRentalDays(2);
            Assert.AreEqual(2*3,rental.CalculateDebt());
        }

        [Test]
        public static void SubstructRentalDays_BigerThanCreated_ThrowException()
        {
            try
            {
                Rental rental = new Rental(new Movie(rentalPrice: 2), days: 6);

                rental.SubstructRentalDays(7);
            }
            catch (RentalDaysException e)
            {
                Assert.AreEqual(-1, e.Days);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        [Test]
        public static void SubstructRentalDays_LessThen0_ThrowException()
        {
            try
            {
                Rental rental = new Rental(new Movie(rentalPrice: 2), days: -6);
            }
            catch (RentalDaysException e)
            {
                Assert.AreEqual(-6, e.Days);
            }
            catch (Exception)
            {
                Assert.Fail();  
            }
        }
    }
}